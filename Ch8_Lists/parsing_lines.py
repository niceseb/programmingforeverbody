__author__ = 'minminsanjose'

# Usually when we are reading a file we want to do something to the lines other than
# just printing the whole line. Often we want to find the 'interesting lines' and then
# parse the line to find some interesting part of the line. What if we wanted to print
# out the day of the week from those lines that start with 'From'.

# From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008 (get Sat)

# The split method is very effective when faced with this kind of problem. We can
# write a small program that looks for lines where the line starts with 'From ' and
# then split those lines and then print out the third word in the line:

fhand = open('../Ch7_Files/mbox-short.txt')

try:
    for line in fhand:

        if len(line) == 0 and not line.startswith('From'):
            continue
        line = line.rstrip()
        words = line.split()
        print words[2]
except IndexError:
    print "Found 'From', but did not find day in the same sentence"


