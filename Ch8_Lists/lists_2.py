__author__ = 'minminsanjose'

# A string is a sequence of characters and a list is a sequence of values, but a list
# of characters is not the same as a string. To convert from a string to a list of
# characters, you can use list:

s = 'spam'
t = list(s)
print t

# list breaks a string into letters
# split breaks a string into words

s = 'pining for the fjords'
t = s.split()
print t