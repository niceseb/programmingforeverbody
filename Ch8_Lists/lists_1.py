__author__ = 'minminsanjose'

# Traversing a list and modifying

# pop, del remove

import numpy

numbers = [17, 5]

for i in range(len(numbers)):
    numbers[i] *= 2
    print numbers

t = ['a', 'd', 'e', 'a', 'd', 'c']

t.sort()

print t

nums = [3, 41, 12, 9, 74, 15]
print 'Given original list of: %s' % nums
print 'Length of nums list is: %s' % len(nums)
print 'Max num from list is: %s' % max(nums)
print 'Min num from list is: %s' % min(nums)
print 'Sum from list is: %s' % sum(nums)
print 'Average from list is: %s' % float(sum(nums)/len(nums))
print 'Average from list using numpy.mean is: %s' % numpy.mean(nums)

