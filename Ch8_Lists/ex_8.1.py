__author__ = 'minminsanjose'

# Write a function called chop that takes a list and modifies it, removing
# the first and last elements,and returns None
#
# Then write a function called middle that takes a list and returns a new list
# that contains all but the first and last elements

import copy

t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

# del takes out element, doesn't return elements
# pop takes out element and returns the taken out element
# remove element by value when index is not known

print 'Original list is: %s' % t


def chop(mylist):
    mylist.__delslice__(0, 1)  # Use of negative indices is not supported
    mylist.pop(-1)
    return None

chop(t)
print 'Original list now modified by chop function: %s' %t


def middle(mylist):
    # newlist = mylist #this aliases
    newlist = copy.deepcopy(mylist)
    newlist.__delslice__(0, 1)  # Use of negative indices is not supported
    newlist.pop(-1)
    return newlist

newlist = middle(t)
print 'Creating a new list from middle function: %s' %newlist
print 'Original list now modified by chop function: %s' %t