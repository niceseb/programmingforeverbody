__author__ = 'minminsanjose'

# Write a program to open the file romeo.txt and read it line by line. For each line,
# split the line into a list of words using the split function.

# For each word, check to see if the word is already in a list. If the word is not in the
# list, add it to the list.

# When the program completes, sort and print the resulting words in alphabetical
# order.

# Enter file: romeo.txt
# ['Arise', 'But', 'It', 'Juliet', 'Who', 'already',
# 'and', 'breaks', 'east', 'envious', 'fair', 'grief',
# 'is', 'kill', 'light', 'moon', 'pale', 'sick', 'soft',
# 'sun', 'the', 'through', 'what', 'window',
# 'with', 'yonder']

# http://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python
# http://www.tutorialspoint.com/python/list_sort.htm
# http://stackoverflow.com/questions/12878833/python-unique-list-using-set

import itertools
from collections import OrderedDict

list2d = []
fhand = open('romeo.txt')

for line in fhand:
    # print lines
    list2d.append(line.split())
    # print words

print 'List from reading romeo.txt is: %s' % list2d
merged = list(itertools.chain.from_iterable(list2d))
print 'Flattened list using itertools: %s' % merged
merged.sort()
print 'Flattened and sorted: %s' % merged
print 'Flattened and sorted and unique: %s' % OrderedDict.fromkeys(merged).keys()
