__author__ = 'minminsanjose'

# Write a simple program to simulate the operation of the grep command
# on Unix. Ask the user to enter a regular expression and count the number of
# lines that matched the regular expression:
#
# """
# $ grep '<caret> From:' mbox-short.txt
# From: stephen.marquard@uct.ac.za
# From: louis@media.berkeley.edu
# From: zqian@umich.edu
# From: rjlowe@iupui.edu
# """
# $ python grep.py
# Enter a regular expression: '^Author'
# mbox.txt had 1798 lines that matched caret Author
#
# $ python grep.py
# Enter a regular expression: '^Xmbox'.
# txt had 14368 lines that matched caret X-

# $ python grep.py
# Enter a regular expression: java$
# mbox.txt had 4218 lines that matched java$

import re
from collections import Counter
import itertools

list2d = []

user_input = raw_input('Enter a regular expression: ')

try:
    fhand = open('../Ch7_Files/mbox-short.txt') # mbox-short
except IOError:
    print 'No file found'

for line in fhand:
    list2d.append(re.findall(user_input, line))

print 'Raw input list: %s' % list2d

mergedlist = list(itertools.chain.from_iterable(list2d))

print 'Flattened list: %s' % mergedlist

a = Counter(mergedlist)

print a

for x in a.most_common(10):
    print x
# print 'Found %s lines that matched %s' % (x, user_input)