__author__ = 'minminsanjose'

# Write a program to look for lines of the form
# New Revision: 39772

# And extract the number from each of the lines using a regular expression and
# the findall() method. Compute the average of the numbers and print out the
# average.

# Enter file:mbox.txt
# 38549.7949721

# Enter file:mbox-short.txt
# 39756.9259259

import itertools
import re
import numpy
from collections import Counter

list2d = []

try:
    fhand = open('../Ch7_Files/mbox-short.txt')
except IOError:
    print 'No file found'

for line in fhand:
    list2d.append(re.findall('^New Revision: ([0-9][0-9][0-9][0-9][0-9])', line))

print 'Raw input is: %s' % list2d

flatlist = list(itertools.chain.from_iterable(list2d))

print 'Flattened list: %s' % flatlist

a = Counter(flatlist)

# print numpy.mean(a.most_common([0]))

revisionlist = []

for x in a.most_common():
    revisionlist.append(x[0])


print 'Revision list: %s' % revisionlist

floatnumlist = []

for x in revisionlist:
    floatnumlist.append(float(x))

print floatnumlist

print 'Ch1_Numpy mean of revision list is: %f' % numpy.mean(floatnumlist)