__author__ = 'minminsanjose'

# This program counts the distribution of the hour of the day for
# each of the messages. You can pull the hour from the 'From ' line by finding the
# time string and then splitting that string into parts using the colon character. Once
# you have accumulated the counts for each hour, print out the counts, one per line,
# sorted by hour as shown below.

# Sample Execution:
# python timeofday.py
# Enter a file name: mbox-short.txt
# 04 3
# 06 1
# 07 1
# 09 2
# 10 3
# 11 6
# 14 1
# 15 2
# 16 4
# 17 2
# 18 1
# 19 1

import string
import itertools
from collections import Counter

list2d = []

try:
    fhand = open('../Ch7_files/mbox-short.txt')
except IOError:
    print 'No file found'

for line in fhand:
    if not line.startswith('From '):  # space to exclude From: lines
        continue
    list2d.append(line.lower().split())  # translate(None, string.punctuation)

# print list2d

flattenedlist = list(itertools.chain.from_iterable(list2d))

print 'Flattened list: %s' % flattenedlist

time = [x for x in flattenedlist if x.__contains__(':')]
print 'Time is: %s' % time

hours = []
for x in time:
    hours.append(x[:2])

print 'Extracted hours: %s' % hours

a = Counter(hours)
#
print 'Counter output: %s' % a
n = 24
#
for x in a.most_common(n):
    print 'Hours: %s, frequency: %s' % (x[0], x[1])