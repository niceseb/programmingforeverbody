__author__ = 'minminsanjose'

# http://stackoverflow.com/questions/20316299/formatting-output-of-counter
# https://gist.github.com/bradmontgomery/4717521

# Coming back to our running example of the text from Romeo and Juliet Act 2,
# Scene 2, we can augment our program to use this technique to print the ten most
# common words in the text as follows:

# 61 i
# 42 and
# 40 romeo
# 34 to
# 34 the
# 32 thou
# 32 juliet
# 30 that
# 29 my
# 24 thee

import string
import itertools
from collections import Counter

try:
    fhand = open('../Ch9_Dictionaries/romeo.txt')
except IOError:
    print 'No file found.'

mylist = []

for line in fhand:
    words = line.translate(None, string.punctuation).lower().split()
    mylist.append(words)

print 'After split, lower-cased: %s' % mylist

mergedlist = list(itertools.chain.from_iterable(mylist))
print 'After flattened is: %s' % mergedlist

# print Counter(mylist) # Counter cannot process nested list?
a = Counter(mergedlist)

print 'After Countered is: %s' % a

n = 10

print("The Top {0} words".format(n))
for x in a.most_common(n):
    print x[1], x[0]