__author__ = 'minminsanjose'

# Write a program that reads a file and prints the letters in decreasing
# order of frequency. Your program should convert all the input to lower case and
# only count the letters a-z. Your program should not count spaces, digits, punctuation
# or anything other than the letters a-z. Find text samples from several different
# languages and see how letter frequency varies between languages. Compare your
# results with the tables at wikipedia.org/wiki/Letter_frequencies.

import itertools
from collections import Counter

import string
try:
    fhand = open('../Ch7_Files/mbox-short.txt')
except IOError:
    print 'No file found.'
mylist = []
for line in fhand:
    words = line.translate(None, string.punctuation).lower().split()
    mylist.append(words)

print 'Raw input: %s' % mylist

flatlist = list(itertools.chain.from_iterable(mylist))

print 'Flattened list: %s' % flatlist

longSingleString = ','.join(flatlist)
print 'Long single comma separated string: %s' % longSingleString

lettersOnly = [x for x in longSingleString if x.isalpha()]

print 'Letters Only: %s' % lettersOnly

a = Counter(lettersOnly)

n = 26

print 'Letters: Frequency'

for x in a.most_common(n):
    print '%s : %s' % (x[0], x[1])

