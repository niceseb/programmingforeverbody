__author__ = 'minminsanjose'

# Q2
# Which of the following methods work both
# in Python lists and Python tuples?

a = list()
b = tuple()

# index()
# pop()
# reverse()
# append()
# sort()

a.index()
b.index()




# Q5

# Which of the following tuples is greater than x in the following Python sequence?

x = (5, 1, 3)

a = 0, 1000, 2000
b = 5, 0, 300
c = 6, 0, 0
d = 4, 100, 200

if c > x:
    print 'True a is greater than x'

else:
    print 'False'


# Q5
# What does the following Python code accomplish, assuming the 'c'
# is a non empty dictionary

c = {'min': 1, 'zahara': 2, 'tony': 3}
tmp = list()

for k, v in c.items():  # c.items will give a list of tuples
    tmp.append((v, k))

print tmp

# ans
# [(3, 'tony'), (2, 'zahara'), (1, 'min')]


# Q7
# If the variable data is a Python list, how do
# we sort it in reverse order?

data = [1, 3, 54, 4546, 7, 8]
tmp = data.reverse()
print tmp

# Q8
# Using the following tuple, how would you print 'Wed'
days = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun')

print days[2]

# Q9
# In the following Python loop, why are there two iteration variables (k and v)?

c = {'a': 10, 'b': 2, 'c': 22, }
for k, v in c.items():
    print k, v

# Q10
# Given that Python lists and Python tuples are quite similar - when
# might you prefer to use a tuple over a list?

