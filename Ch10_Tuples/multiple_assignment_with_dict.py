__author__ = 'minminsanjose'

# Combining items, tuple assignment and for, you can see a nice code pattern for
# traversing the keys and values of a dictionary in a single loop:

d = {'a': 10, 'b': 1, 'c': 22}

l = list()

for key, val in d.items():
    # print val, key
    l.append((val, key))


print l

l.sort(reverse=True)

print l