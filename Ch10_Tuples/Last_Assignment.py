__author__ = 'minminsanjose'

# 10.2 Write a program to read through the mbox-short.txt and figure out the distribution
# by hour of the day for each of the messages. You can pull the hour out from the 'From '
# line by finding the time and then splitting the string a second time using a colon.

mylist = []

name = raw_input("Enter file:")
if len(name) < 1:
    name = "../Ch7_Files/mbox-short.txt"

handle = open(name)

for line in handle:
    if not line.startswith('From '):
        continue
    # print mylist.append(line)
    time = line.rstrip().split()[5]
    mylist.append(time)

print mylist
print mylist[0]
print mylist[1]

secondlist = []

for hour in mylist:
    secondlist.append(hour[0:2])

print secondlist

# print dict((i, secondlist.count(i) for i in secondlist))
# http://stackoverflow.com/questions/2600191/how-can-i-count-the-occurrences-of-a-list-item-in-python

def occurDict(items):
    d = {}
    for i in items:
        if i in d:
            d[i] += 1
        else:
            d[i] = 1
    return d

# print occurDict(secondlist)
# print type(line.rstrip().split()[5].split())
# http://stackoverflow.com/questions/9001509/python-dictionary-sort-by-key

mydict = occurDict(secondlist)
keylist = mydict.keys()
keylist.sort()
for key in keylist:
    print "%s %s" % (key, mydict[key])