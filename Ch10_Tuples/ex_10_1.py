__author__ = 'minminsanjose'

# Revise a previous program as follows: Read and parse the 'From'
# lines and pull out the addresses from the line. Count the number of messages from
# each person using a dictionary.

# After all the data has been read print the person with the most commits by creating
# a list of (count, email) tuples from the dictionary and then sorting the list in reverse
# order and print out the person who has the most commits.

# Sample Line:
# From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

# Enter a file name: mbox-short.txt
# cwen@iupui.edu 5

# Enter a file name: mbox.txt
# zqian@umich.edu 195

import string
import itertools
from collections import Counter

list2d = []

try:
    fhand = open('../Ch7_files/mbox-short.txt')
except IOError:
    print 'No file found'

for line in fhand:
    if not line.startswith('From '):  # space to exclude From: lines
        continue
    list2d.append(line.lower().split())  # translate(None, string.punctuation)

# print list2d

flattenedlist = list(itertools.chain.from_iterable(list2d))

print flattenedlist

emails = [x for x in flattenedlist if x.__contains__('@')]
# print emails

a = Counter(emails)

# print a
n = 5

for x in a.most_common(n):
    print x



