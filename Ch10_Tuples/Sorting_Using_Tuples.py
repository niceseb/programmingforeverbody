__author__ = 'minminsanjose'

# DSO

# Decorate a sequence by building a list of tuples with one or more sort keys preceding
# the elements from the sequence,

# Sort the list of tuples using the Python built-in sort, and

# Undecorate by extracting the sorted elements of the sequence.

# For example, suppose you have a list of words and you want to sort them from
# longest to shortest:

# Given txt = 'but soft what light in yonder window breaks'

# The output of the program is as follows:
# ['yonder', 'window', 'breaks', 'light', 'what',
# 'soft', 'but', 'in']

txt = 'but soft what light in yonder window breaks'
words = txt.split()
print 'List of words after split function: %s' % words

t = list()

for word in words:
    t.append((len(word), word))

print 'Building a list of tuples with length keys: %s' % t

t.sort(reverse=True)

print 'List of words after sorting: %s' % t

res = list()

for length, word in t:
    res.append(word)

print 'List of words after undecorating by extracting the length info: %s' % res