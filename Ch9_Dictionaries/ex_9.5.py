__author__ = 'minminsanjose'


# This program records the domain name (instead of the address)
# where the message was sent from instead of who the mail came from (i.e. the
# whole e-mail address). At the end of the program print out the contents of your
# dictionary.

# python schoolcount.py
# https://bitbucket.org/shantanoo/python/src/79632f2c0e51430d02435272b9af4f7e83bf7544/EmailExtractor.py?at=master
# Enter a file name: mbox-short.txt
# {'media.berkeley.edu': 4, 'uct.ac.za': 6, 'umich.edu': 7,
# 'gmail.com': 1, 'caret.cam.ac.uk': 1, 'iupui.edu': 8}

# https://developers.google.com/edu/python/regular-expressions
# http://www.decalage.info/en/python/print_list

import itertools
from collections import Counter
import re

words = []

try:
    fhand = open('../Ch7_files/mbox-short.txt')
except IOError:
    print 'No file found'

for line in fhand:
    if not line.startswith('From'):
        continue
    line.rstrip()
    words.append(line.split())

print 'Before flattening is: %s' % words
merged = list(itertools.chain.from_iterable(words))
print 'After flattening is: %s' % merged

myDictList = [x for x in merged if (x.__contains__('@'))]
print 'Filtered for emails only: %s' % myDictList

emailStr = ', '.join(myDictList)
print 'As a single long string of emails: %s' % emailStr

list_Separated_Emails_Hosts = re.findall(r'([\w.-]+)@([\w.-]+)', emailStr)

print 'List of separated emails and hosts: %s' % list_Separated_Emails_Hosts

hostDict = []

for host in list_Separated_Emails_Hosts:
    hostDict.append(host[1])
    print host[1]

print 'List of hosts: %s' % hostDict

print 'Host recurrences : %s' % Counter(hostDict)