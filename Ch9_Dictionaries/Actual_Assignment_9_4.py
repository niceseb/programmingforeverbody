__author__ = 'minminsanjose'

# 9.4
# Write a program to read through the mbox - short.txt and figure out who has the sent the greatest number of
# mail messages.The program looks for 'From ' lines and takes the second word of those lines as the person
# who sent the mail.The program creates a Python dictionary that maps the sender's mail address to a count
# of the number of times they appear in the file. After the dictionary is produced, the program reads
# through the dictionary using a maximum loop to find the most prolific committer.

selectedLineFromText = list()
name = raw_input('Enter file:')

if len(name) < 1:
    name = '../Ch7_Files/mbox-short.txt'

handle = open(name)

for line in handle:
    if not line.startswith('From '):
        continue
    selectedLineFromText.append(line.strip().split())
    # http://stackoverflow.com/questions/13188476/get-the-nth-element-from-the-inner-list-of-a-list-of-lists-in-python
    emailNames = [el[1] for el in selectedLineFromText]

print 'This is selected lines from text : %s' % selectedLineFromText
print 'This is emails only : %s' % emailNames


def occurDict(items):
    d = {}
    for i in items:
        if i in d:
            d[i] += 1
        else:
            d[i] = 1
    return d


d = occurDict(emailNames)
print 'This is from occurDict function ; %s' % d

commits = 0
committer = None

# https://class.coursera.org/pythonlearn-003/forum/thread?thread_id=1934
for key, value in d.items():
    if value > commits:
        committer = key
        commits = value

print 'Committer, commits'
print committer, commits

# http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value?rq=1
# tmp = sorted(d.items(), key=lambda d: d[1])
#
# print type(tmp)
#
# print tmp
# print tmp[-1]


# maxx = max(d.values())
# keys = [x for x,y in d.items() if y==maxx]
#
# print 'This is maxx'
# print keys

#
# inverse = [(value, key) for key, value in d.items()]
# print max(inverse)[1], value

# http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value
# for w in sorted(d, key=d.get, reverse=False):
# pass
# print w, d[w]
#
# print w, d[w]

# for w in sorted(d.items(), key=lambda x: x[1]):
# # pass
# print w, d[w]

# print w, d[w]

# print sorted([(value, key) for (key, value) in d.items()])
# top = sorted([(value, key) for (key, value) in d.items()])[-1]
# print 'Thi is top below:'
# print top
# print top[0]
# print top[1]
#
# new = list()
#
# new.append(top[1])
# new.append(top[0])


# new[0], new[1] = top[1], top[0]

# print new

# print [(t[1], t[0]) for t in top]

# print type(sorted([(value, key) for (key, value) in d.items()])[-1])
# print sorted([(value, key) for (value, key) in d.items()])
# print type(top)

# def myFunction(emailList):
# for email in emailList:
# return email[1]
#
# print 'This is emails only : %s' %myFunction(emailList)
# print 'This is flattenList : %s' %flattenList(emailList)
# http://stackoverflow.com/questions/2600191/how-can-i-count-the-occurrences-of-a-list-item-in-python







