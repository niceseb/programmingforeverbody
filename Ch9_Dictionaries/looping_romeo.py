__author__ = 'minminsanjose'

import itertools
from collections import OrderedDict

# mylist = {'chuck': 1, 'annie': 42, 'jan': 100}
# for key in mylist:
#     print key, mylist[key]

# Write a parser to read in romeo.txt and see a raw dump of all the counts in unsorted order
# Enter the file name: romeo.txt
# {'and': 3, 'envious': 1, 'already': 1, 'fair': 1,
# 'is': 3, 'through': 1, 'pale': 1, 'yonder': 1,
# 'what': 1, 'sun': 2, 'Who': 1, 'But': 1, 'moon': 1,
# 'window': 1, 'sick': 1, 'east': 1, 'breaks': 1,
# 'grief': 1, 'with': 1, 'light': 1, 'It': 1, 'Arise': 1,
# 'kill': 1, 'the': 3, 'soft': 1, 'Juliet': 1}

list2d = []

try:
    fhand = open('romeo.txt')
except IOError:
    print 'File cannot be found'
    exit()

for line in fhand:
    list2d.append(line.split())
    # print list2d

merged = list(itertools.chain.from_iterable(list2d))
merged.sort()
flattenedlist = OrderedDict.fromkeys(merged).keys()
print 'Flattened and sorted and unique: %s' % flattenedlist
converted_dict = dict.fromkeys(flattenedlist, 0)
print 'Converted to dictionary: %s' % converted_dict

mydict = dict()

for pointer in flattenedlist:  # converted_dict
    mydict[pointer] = mydict.get(pointer, 0) + 1
print 'Unsorted hash order: %s' % mydict
