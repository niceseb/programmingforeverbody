__author__ = 'minminsanjose'

import itertools
from collections import OrderedDict

# mylist = {'chuck': 1, 'annie': 42, 'jan': 100}
# for key in mylist:
#     print key, mylist[key]

# Write a parser to read in romeo.txt and see a raw dump of all the counts in unsorted order
# Enter the file name: romeo.txt
# {'and': 3, 'envious': 1, 'already': 1, 'fair': 1,
# 'is': 3, 'through': 1, 'pale': 1, 'yonder': 1,
# 'what': 1, 'sun': 2, 'Who': 1, 'But': 1, 'moon': 1,
# 'window': 1, 'sick': 1, 'east': 1, 'breaks': 1,
# 'grief': 1, 'with': 1, 'light': 1, 'It': 1, 'Arise': 1,
# 'kill': 1, 'the': 3, 'soft': 1, 'Juliet': 1}

import string  # New Code

fname = raw_input('Enter the file name: ')
try:
    fhand = open(fname)

except IOError:
    print 'File cannot be opened:', fname
    exit()

counts = dict()
for line in fhand:
    line = line.translate(None, string.punctuation)  # New Code
    line = line.lower()  # New Code
    words = line.split()
    for word in words:
        if word not in counts:
            counts[word] = 1
        else:
            counts[word] += 1
print counts