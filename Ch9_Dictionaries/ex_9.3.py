__author__ = 'minminsanjose'

# Write a program to read through a mail log, and build a histogram
# using a dictionary to count how many messages have come from each email address
# and print the dictionary.

# Enter file name: mbox-short.txt

# {'gopal.ramasammycook@gmail.com': 1, 'louis@media.berkeley.edu': 3,
# 'cwen@iupui.edu': 5, 'antranig@caret.cam.ac.uk': 1,
# 'rjlowe@iupui.edu': 2, 'gsilver@umich.edu': 3,
# 'david.horwitz@uct.ac.za': 4, 'wagnermr@iupui.edu': 1,
# 'zqian@umich.edu': 4, 'stephen.marquard@uct.ac.za': 2,
# 'ray@media.berkeley.edu': 1}

import itertools
from collections import Counter

words = []
try:
    # fhand = open('../Ch7_Files/mbox.txt')
    fhand = open('../Ch7_Files/mbox-short.txt')

except IOError:
    print 'File cannot be found'
    exit()

for line in fhand:
    if not line.startswith('From'):
        continue
    line.rstrip()
    words.append(line.split())

print 'Before flattening is: %s' % words
merged = list(itertools.chain.from_iterable(words))
print 'After flattening is: %s' % merged

myOnDict = [x for x in merged if (x.__contains__('@'))]

print 'Filtered for emails only: %s' % myOnDict
print 'Histogram :%s' % Counter(myOnDict)
print 'Most messages: %s' %Counter(myOnDict).most_common()