__author__ = 'minminsanjose'

# Suppose you are given a string and you want to count how many times each letter
# appears. There are several ways you could do it:

# http://shakespeare.mit.edu/Tragedy/romeoandjuliet/romeo_juliet.2.2.html

word = 'brontosaurus'
mydict = dict()
for pointer in word:
    if pointer not in mydict:
        mydict[pointer] = 1
    else:
        mydict[pointer] += 1
print mydict

# We can use get to write our histogram loop more concisely. Because the get
# method automatically handles the case where a key is not in a dictionary, we can
# reduce four lines down to one and eliminate the if statement

# Better way
word = 'brontosaurus'
mydict = dict()
for pointer in word:
        mydict[pointer] = mydict.get(pointer, 0) + 1
print mydict
