__author__ = 'minminsanjose'


# https://docs.python.org/3/library/stdtypes.html#dict.get

stuff = dict()
print stuff.get('candy', -1)



# Which of the following lines of Python is equivalent
# to the following sequence of statements assuming that counts
# is a dictionary

counts = dict()

# print type(counts)

key = 0

if key in counts:
    counts[key] += 1
    print 'first line : %s ' % counts[key]

else:
    counts[key] = 1
    print 'second line : %s ' % counts[key]
