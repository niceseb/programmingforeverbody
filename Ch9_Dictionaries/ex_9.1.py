__author__ = 'minminsanjose'

# Write a program that reads the words in words.txt and stores them as keys in a dictionary.
# It doesn't matter what the values are. Then you can use the in operator as a fast way to check
# whether a string is in the dictionary

# http://stackoverflow.com/questions/3869487/how-do-i-create-a-dictionary-with-keys-from-a-list-and-values-defaulting-to-say

import itertools
from collections import OrderedDict

list2d = []
fhand = open('words.txt')

for line in fhand:
    list2d.append(line.split())

print 'List from reading romeo.txt is: %s' % list2d
merged = list(itertools.chain.from_iterable(list2d))
print 'Flattened list using itertools: %s' % merged
merged.sort()
print 'Flattened and sorted: %s' % merged

tmp = OrderedDict.fromkeys(merged).keys()
print 'Flattened and sorted and unique: %s' % tmp

mydict = dict.fromkeys(tmp, 0)
print 'Converted to dictionary: %s' % mydict

if 'only' in mydict:
    print 'Found it'
else:
    print 'Nothing in dictionary to translate'