__author__ = 'minminsanjose'
# Write a program that categorizes each mail message by which day of
# the week the commit was done. To do this look for lines which start with 'From',
# then look for the third word and then keep a running count of each of the days
# of the week. At the end of the program print out the contents of your dictionary
# (order does not matter).

# Sample Line:
# From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

# Sample Execution:
# python dow.py
# Enter a file name: mbox-short.txt
# {'Fri': 20, 'Thu': 6, 'Sat': 1}

# http://stackoverflow.com/questions/16304146/in-python-how-do-i-extract-a-sublist-from-a-list-of-strings-by-matching-a-strin
# http://stackoverflow.com/questions/2600191/how-can-i-count-the-occurrences-of-a-list-item-in-python
# http://stackoverflow.com/questions/5352546/best-way-to-extract-subset-of-key-value-pairs-from-python-dictionary-object
from collections import Counter
import itertools
from collections import OrderedDict

words = []

try:
    fhand = open('../Ch7_Files/mbox-short.txt')
except IOError:
    print 'File cannot be found'
    exit()

for line in fhand:
    if not line.startswith('From'):
        continue
    line.rstrip()
    words.append(line.split())

print 'before flattening is: %s' % words
merged = list(itertools.chain.from_iterable(words))
print 'after flattening is: %s' % merged

# print merged.count('Fri')
# print merged.count('Sat')
# print merged.count('Sun')
# print merged.count('Mon')
# print merged.count('Tue')
# print merged.count('Wed')
# print merged.count('Thu')

myOnDict = [x for x in merged if (x.__contains__('Thu') or x.__contains__('Fri')
                                  or x.__contains__('Sat') or x.__contains__('Sun')
                                  or x.__contains__('Mon') or x.__contains__('Tue')
                                  or x.__contains__('Wed'))]
print myOnDict
tmp = Counter(myOnDict)
print tmp


