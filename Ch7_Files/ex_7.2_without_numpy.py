__author__ = 'minminsanjose'

# Coursera Actual Question
# Use the file name mbox-short.txt as the file name
# http://stackoverflow.com/questions/9039961/finding-the-average-of-a-list
# l = [15, 18, 2, 36, 12, 78, 5, 6, 9]
# print reduce(lambda x, y: x + y, l) / len(l)
# ans
fname = raw_input("Enter file name: ")  # mbox-short.txt
lst = []

fh = open(fname)
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:"):
        continue
    at_pos = line.find('Confidence:')
    extracted_data = float(line[at_pos + 11:])
    # print extracted_data
    lst.append(extracted_data)
    #print line
tmp = reduce(lambda x, y: x + y, lst) / len(lst) # same
# tmp = sum(lst)/len(lst)
print "Average spam confidence: %s" % tmp