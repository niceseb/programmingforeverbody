__author__ = 'minminsanjose'

import logging
import numpy

# Write a program to prompt for a file name, and then read through
# the file and look for lines of the form:
# X-DSPAM-Confidence: 0.8475

# from ex_6.5

# You can download the file from www.py4inf.com/code/mbox-short.txt

fname = raw_input('Enter file to open:')
average_confidence = []

# if user_file == 'na na boo boo':
# print "%s TO YOU - You have been punk'd!" %user_file.upper()

try:
    fh = open(fname)
    for line in fh:
        if line.__contains__('Confidence:'):
            atpos = line.find('Confidence:')
            logging.debug('Position is at: %s' % atpos)
            print 'Position is at: %s' % atpos
            extracted_data = line[atpos + 11:]
            print 'Extracted float data is: %s' % float(extracted_data)
            average_confidence.append(float(extracted_data))
    print 'Average spam confidence: %s' % numpy.mean(average_confidence)
except IOError:
    print 'And %s too, have a nice day!' % fname



