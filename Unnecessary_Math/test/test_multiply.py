# from unittest import TestCase
from Unnecessary_Math.main.unnecessary_math import *


def test_numbers_3_4():
    assert multiply(3, 4) == 12


def test_numbers_a_3():
    assert multiply('a', 3) == 'aaa'


def test_numbers_b_3():
    assert multiply('b', 3) == 'bbb'