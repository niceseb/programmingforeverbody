__author__ = 'minminsanjose'

import re
import urllib
from BeautifulSoup import *

url = raw_input('Enter - ')  # http://www.py4inf.com/book.htm
html = urllib.urlopen(url).read()

# links = re.findall('href="(http://.*?)"', html)
# for link in links:
#     print link

# Replaced with BeautifulSoup to extract href attributes
soup = BeautifulSoup(html)

# Retrieve all of the anchor tags
tags = soup('a')
for tag in tags:
    # Look at the parts of a tag
    # print tag.get('href', None)
    print 'TAG:', tag
    print 'URL:', tag.get('href', None)
    print 'Content:', tag.contents[0]
    print 'Attrs:', tag.attrs