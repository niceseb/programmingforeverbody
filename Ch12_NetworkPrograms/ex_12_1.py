__author__ = 'minminsanjose'

# Change the socket program socket1.py to prompt the user for the
# URL so it can read any web page. You can use split "('/')" to break the URL into
# its component parts so you can extract the host name for the socket connect call.

# Add error checking using try and except to handle the condition where the user
# enters an improperly formatted or non-existent URL.

# Original:
# import socket
# mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# mysock.connect(('www.py4inf.com', 80))
# mysock.send('GET http://www.py4inf.com/code/romeo.txt HTTP/1.0\n\n')
# while True:
#     data = mysock.recv(512)
#     if len(data) < 1:
#         break
#     print data
# mysock.close()

# Come back to it when Coursera starts

from bs4 import BeautifulSoup
import requests

import socket
from urlparse import urlparse  # new

url = raw_input("Enter a website to extract the URL's from:")
r = requests.get('http://' + url)
data = r.text
soup = BeautifulSoup(data)
for link in soup.find_all('a'):
    print(link.get('href'))

# o = urlparse(my_url)
# mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# mysock.connect((o.hostname, 80))  # o.port # 'www.py4inf.com'
# mysock.send("GET o.scheme+'//'+o.hostname HTTP/1.0\n\n")
#
# while True:
#     data = mysock.recv(512)
#     if len(data) < 1:
#         break
#     print data
#
# mysock.close()