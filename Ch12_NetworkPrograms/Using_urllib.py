__author__ = 'minminsanjose'

# While we can manually send and receive data over HTTP using the socket library,
# there is a much simpler way to perform this common task in Python by using the
# urllib library.
# Using urllib, you can treat a web page much like a file. You simply indicate
# which web page you would like to retrieve and urllib handles all of the HTTP
# protocol and header details.
# The equivalent code to read the romeo.txt file from the web using urllib is as
# follows:

import urllib

counts = {}

fhand = urllib.urlopen('http://www.py4inf.com/code/romeo.txt')
# for line in fhand:
#     print line.strip()
#
# print line

for lines in fhand:
    words = lines.split()
    for word in words:
        counts[word] = counts.get(word, 0) + 1
print counts
