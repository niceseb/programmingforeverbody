__author__ = 'minminsanjose'

import socket
import time

count = 0
picture = ""

try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysock.connect(('www.py4inf.com', 80))
    mysock.send('GET http://www.py4inf.com/cover.jpg HTTP/1.0\n\n')

    while True:
        data = mysock.recv(5120)
        if len(data) < 1:
            break
        time.sleep(0.25)  # delay so the server can get a 0.25s to send more data
        count += len(data)
        print len(data), count
        picture += data

    mysock.close()

    # Look for the end of the header(2 CRLF)
    pos = picture.find("\r\n\r\n")
    print 'Header length', pos
    print picture[:pos]

    # Skip past the header and save the picture data
    picture = picture[pos + 4:]
    fhand = open("stuff.jpg", "wb")
    fhand.write(picture)
    fhand.close()

except socket.gaierror:
    print 'No nodename nor servname provided'
except socket.error:
    print 'Socket is not connected'





