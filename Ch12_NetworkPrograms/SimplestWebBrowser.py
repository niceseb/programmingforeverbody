__author__ = 'minminsanjose'


import socket


try:
    mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mysock.connect(('www.py4inf.com', 80))
    mysock.send('GET http://www.py4inf.com/code/romeo.txt HTTP/1.0\n\n')

    while True:
        data = mysock.recv(512)
        if len(data) < 1:
            break
        print data

    mysock.close()

except socket.gaierror:
    print 'No nodename nor servname provided'
except socket.error:
    print 'Socket is not connected'



