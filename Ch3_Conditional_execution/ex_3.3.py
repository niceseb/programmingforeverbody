from symbol import continue_stmt

__author__ = 'minminsanjose'

# Write a program to prompt for a score between 0.0 and 1.0. If the
# score is out of range print an error. If the score is between 0.0 and 1.0, print a
# grade using the following table:
# Score Grade
# >= 0.9 A
# >= 0.8 B
# >= 0.7 C
# >= 0.6 D
# < 0.6 F
# Enter score: 0.95
# A
# Enter score: perfect
# Bad score
# Enter score: 10.0
# Bad score
# Enter score: 0.75
# C
# Enter score: 0.5
# F
# Run the program repeatedly as shown above to test the various different values for
# input.

user_grades = raw_input('Enter score between 0.0 and 1.0 :')


try:
    grades = float(user_grades)

    if 0 <= grades <= 1:

        if grades >= 0.9:
            print 'A'
        elif grades >= 0.8:
            print 'B'
        elif grades >= 0.7:
            print 'C'
        elif grades >= 0.6:
            print 'D'
        elif grades < 0.6:
            print 'F'
    else:
        print 'Bad score as not in range'

except ValueError:
    print 'Bad score, not numeric number'
