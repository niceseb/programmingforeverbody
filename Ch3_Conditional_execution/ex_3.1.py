__author__ = 'minminsanjose'

# Rewrite your pay computation to give the employee 1.5 times the hourly rate
#for hours worked above 40 hours

#Enter hours: 45
#Enter Rate: 10
#Pay: 475.0

hrs = raw_input('Enter hours:')
hrs_int = float(hrs)

rate = raw_input('Enter Rate:')
rate_float = float(rate)

if hrs_int > 40:
    print 'Pay with overtime: $%s' % float((hrs_int - 40.0) * rate_float * 1.5 + 40.0 * rate_float)
else:
    print 'Normal Pay: $%s' % float(hrs_int * rate_float)