__author__ = 'minminsanjose'

# Rewrite your pay computation to give the employee 1.5 times the hourly rate
#for hours worked above 40 hours

#Enter hours: 45
#Enter Rate: 10
#Pay: 475.0

try:
    user_hours = raw_input('Enter hours:')
    user_rate = raw_input('Enter Rate:')
    hrs_int = float(user_hours)
    rate_float = float(user_rate)
    if hrs_int > 40:
        print 'Pay with overtime: $%s' % float((hrs_int - 40.0) * rate_float * 1.5 + 40.0 * rate_float)
    else:
        print 'Normal Pay: $%s' % float(hrs_int * rate_float)
except ValueError:
    print 'Error, please enter numeric input'
