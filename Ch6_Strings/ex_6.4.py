__author__ = 'minminsanjose'

# There is a string method called count that is similar to the function
# in the previous exercise. Read the documentation of this method at docs.python.
# org/library/string.html and write an invocation that counts the number of
# times the letter a occurs in 'banana'.

word = 'banana'

print "Number of times letter 'a' appears in: %s is %s" % (word, word.count('a'))
