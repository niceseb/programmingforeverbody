__author__ = 'minminsanjose'

# http://stackoverflow.com/questions/931092/reverse-a-string-in-python

index = 0

fruit = raw_input('Enter some texts:')

# this prints forward a letter at a time
# while index < len(fruit):
#     letter = fruit[index]
#     print letter
#     index += 1

# write a loop that starts at the last character in the string
# and works its way backwards to the first character in the string,
# printing each letter on a separate line, except backwards
#

tmp = fruit[::-1]

while index < len(fruit):
    letter = tmp[index]
    print letter
    index += 1

