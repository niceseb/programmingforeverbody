__author__ = 'minminsanjose'

# Q2
# str = "hello there bob"
# print str[0]
# ans h

# Q3
# print "Hello World"

# Q7
# stuff = ['joseph', 'sally', 'walter', 'tim']
# print stuff[2]
# ans walter

# Q11
# st = "abc"
# ix = int(st)
# asn Traceback on second line

# Q12
# lst = []
# lst.append(4)
# lst.append(10)
# lst.append(21)
# lst.append(6)
# print lst[2]
# ans 21

# Q14
# def hello():
# print "Hello"
# print "There"
#
# x = 10
# x = x + 1
#
# ans Nothing will be printed

# Q16
# x = 0
# for value in [3, 41, 12, 9, 74, 15]:
#     if value < 10:
#         x += value
# print x
# ans 12

# Q17
# fline = "blah blah"
# if len(fline) > 1:
#     print "More than one"
#     if fline[0] == 'h':
#         print "Has an h"
# print "All Done"

# abc = 1 - 2 + 3 * 4 - 5 - 6 / 3
# ans 4

# Q20
x = -1
for value in [3, 41, 12, 9, 74, 15]:
    if value < x:
        x = value
print x

ans = -1