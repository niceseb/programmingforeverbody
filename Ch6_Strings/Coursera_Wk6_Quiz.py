__author__ = 'minminsanjose'


# Q2
# x = '40'
# y = int(x) + 2
# print y
# ans 42

# Q3
# How would you use the index operator []
# to print out the letter q from the following string?
# x = 'From marquard@uct.ac.za'
# ans print x[8]

# Q4
# How would you use string slicing[:] to print
# out 'uct' from the following string?
# x = 'From marquard@uct.ac.za'
# ans print x[14:17]

# Q5
# What is the iteration variable in the following
# Python code?
# for letter in 'banana':
#     print letter
# ans letter

# Q6
# What does the following Python code print out?
# print len('banana')*7
# ans 42

# Q7
# How would you print out the following variable
# in all upper case in Python?
# greet = 'Hello Bob'
# ans print greet.upper()

# Q8
# Which of the following is NOT a valid string
# method of Python?
# twist()

# Q9
# Which will the following Python code print out?
# data = 'From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008'
# pos = data.find('.')
# print data[pos:pos+3]
# ans .ma

# Q10
# Which of the following string methods removes whitespace from
# both the beginning and end of a string?
# strip()
# rltrim()
# strtrunc()
# wsrem()

testStr = " Hello "
print len(testStr)
print len(testStr.strip())
