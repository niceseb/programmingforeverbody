__author__ = 'minminsanjose'

# Encapsulate this code in a function named count, and generalize it
# so that it accepts the string and the letter as arguments.

def myCount(word, alphabet):
    # global word, count, letter
    # word = 'banana'
    count = 0
    for letter in word:
        if letter == alphabet:
            count += 1
    return count


test = myCount('Sebastian Zahara', 'a')

print test


