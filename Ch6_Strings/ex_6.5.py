__author__ = 'minminsanjose'

# Take the following Python code that stores a string:
# Use find and string slicing to extract the portion of the string after the colon
# character and then use the float function to convert the extracted string into a
# floating point number.

mystr = 'X-DSPAM-Confidence: 0.8475'

atpos = mystr.find(':')

print atpos

extracted_number = mystr[atpos+1:]

print 'Extracted data is:%s' %extracted_number

print 'After converted to floating point is: %s' %float(extracted_number)