__author__ = 'minminsanjose'

# http://www.pythonlearn.com/book_008.pdf
#Lets say you are doing Social Computing research on Facebook posts and you are
#interested in the most frequently used word in a series of posts.

# name = raw_input('Enter your file:')
# handle = open(name, 'r')
handle = open('../Ch9_Dictionaries/words.txt', 'r')

text = handle.read()
words = text.split()
counts = dict()

for word in words:
    counts[word] = counts.get(word, 0) + 1

bigcount = None
bigword = None
for word, count in counts.items():
    if bigcount is None or count > bigcount:
        bigword = word
        bigcount = count

print bigword, bigcount