__author__ = 'minminsanjose'

# Write a program which prompts the user for a Celsius temperature,
# converts the temperature to Fahrenheit and print out the converted temperature

temperature_from_user = raw_input('Enter temperature in Celsius:')

#MUST REMEMBER DIVISION OF FLOATS
# 9/5 = 1
# 9/5.0 = 1.8
# print float(temperature_from_user)*(2)+30

try:
    print float(temperature_from_user)*(9 / 5.0)+32
except ValueError:
    print 'Please enter a valid number'
