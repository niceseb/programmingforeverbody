__author__ = 'minminsanjose'

# You can extract the right-most digit or digits from a number. For example,
# x % 10 yields the right-most digit of x (in base 10). Similarly x % 100 yields the
# last two digits.

# 499 % 10
#
# ans 9
#
# 499 % 100
#
# ans = 99

words = ['the quick brown fox jumps over the lazy dog jumps over the lazy dog']
for word in words:
    print word

# Order of precedence
# PEMDAS
#
# Parentheses
# Exponential
# Multiplication
# Division
# Addition
# Subtraction

import math
print 1.0 / 2.0 * math.pi
print 1.0 / ( 2.0 * math.pi )