__author__ = 'minminsanjose'

import numpy

# Write a program which repeatedly reads numbers until the user enters 'done'.
# Once 'done' is entered, print out the total, count, and average of the numbers.
# If the user enters anything other than a number, detect their mistake using try
# and except and print an error message and skip to the next number

# Enter a number: 4
# Enter a number: 5
# Enter a number: bad data
# Invalid input
# Enter a number: 7
# Enter a number: done
# 16 3 5.33333333333

total = 0
mylist = []

while True:

    try:
        line = raw_input('Enter a number:  ')
        if line.isdigit():
            mylist.append(int(line))
            total += int(line)
    except ValueError:
        print 'Invalid input'

# The code works fine until it is presented an empty line. Then there is no zeroth
# character so we get a traceback. There are two solutions to this to make line three
# 'safe' even if the line is empty.
# One possibility is to simply use the startswith method which returns False if
# the string is empty

# Another way to safely write the if statement using the guardian pattern and make
# sure the second logical expression is evaluated only where there is at least one
# character in the string.:

    # if line[0] == '#':
    if len(line) > 0 and line[0] == '#':
    # if line.startswith('#'):
        continue

    if line == 'done':
        print 'Total: %s' %total, 'Count: %s' %len(mylist), 'Average: %s' %numpy.mean(mylist), 'Min: %s' %numpy.min(mylist), 'Max: %s' %numpy.max(mylist)
        break

print 'Done!'