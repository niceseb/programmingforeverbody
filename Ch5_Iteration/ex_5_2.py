__author__ = 'minminsanjose'

# Actual Coursera
# http://stackoverflow.com/questions/8525447/python-max-and-min
# http://stackoverflow.com/questions/6117733/negation-in-python
# passed

num_list = []

while 1:
    number = raw_input("Please enter a number: ")

    # Handle edge cases
    if number == "done": break
    if not number.isdigit():
        print 'Invalid input'
        continue

    # Do the work
    try:
        num_list.append(number)
    except:
        print 'Invalid input'
        continue

    high = max(num_list)
    low = min(num_list)

print "Maximum is %s" % high
print "Minimum is %s" % low


def totalList():
    global tot, i
    tot = 0
    for i in [5, 4, 3, 2, 1]:
        tot = tot + 1
    print tot


totalList()


def smallSoFar():
    global smallest_so_far, the_num
    smallest_so_far = -1
    for the_num in [9, 41, 12, 3, 74, 15]:
        if the_num < smallest_so_far:
            smallest_so_far = the_num
    print smallest_so_far


smallSoFar()

# The is operator is stronger than the equality operator( ==) as it insists on matching the two values
# exactly including type.This simple example shows the difference:
# >>> 1.0 == 1
# True
# >>> 1.0 is 1
# False
# While
# 1.0 is the same value after the integer 1 is converted to floating point, the 'is' operator does no
# conversion and so the two values do not match. The 'is' operator is best
# used on small constant values like small integers, True, False, and None. The 'is' operator should
# not be used with large numeric values or strings - these values should be compared with the == operator.

if smallest is None:
    smallest = value

    Question
    Explanation