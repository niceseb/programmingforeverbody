__author__ = 'minminsanjose'

#Definitions of functions can be any order, but the call has to be after functions are already defined


def print_lyrics():
    print "I'm a lumberjack, and I'm okay."
    print 'I sleep all night and I work all day.'


def repeat_lyrics():
    print_lyrics()
    print_lyrics()


repeat_lyrics()
