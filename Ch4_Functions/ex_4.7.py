__author__ = 'minminsanjose'

# Rewrite ex_3.3 into function called computegrade that takes a score as its parameter and returns a grade
# as a string

user_grades = raw_input('Enter score between 0.0 and 1.0 :')


def computegrade(grades):
    cal_grade = ''
    try:
        grades = float(grades)

        if 0 <= grades <= 1:

            if grades >= 0.9:
                cal_grade = 'A'
            elif grades >= 0.8:
                cal_grade = 'B'
            elif grades >= 0.7:
                cal_grade = 'C'
            elif grades >= 0.6:
                cal_grade = 'D'
            elif grades < 0.6:
                cal_grade = 'F'
        else:
            print 'Bad score as not in range'

    except ValueError:
        print 'Bad score, not numeric number'

    return cal_grade

grade = computegrade(user_grades)
print grade
