__author__ = 'minminsanjose'

# Rewrite your pay computation with time-and-a-half for overtime
# and create a function called computepay which takes two parameters (hours and
# rate).
# Enter Hours: 45
# Enter Rate: 10.5
# Pay: 475.0
# def computepay(hours, rate):


def computepay():
    user_hours = raw_input('Enter hours:')
    user_rate = raw_input('Enter Rate:')
    hrs_float = float(user_hours)
    rate_float = float(user_rate)

    if hrs_float > 40:
        pay = float((hrs_float - 40.0) * rate_float * 1.5 + 40.0 * rate_float)
        print 'Pay with overtime: $%s' % pay
    else:
        pay = float(hrs_float * rate_float)
        print 'Normal Pay: $%s' % pay

    return pay
# computepay(user_hours,user_rate)
computepay()