from unittest import TestCase
# https://confluence.jetbrains.com/display/PYH/Creating+and+running+a+Python+unit+test

__author__ = 'minminsanjose'

from Ch4_Functions import

class TestComputepay(TestCase):
    def test_computepay(self):
        c = computepay()
        self.assertEqual(c(45, 10.5) == 498.75)


if __name__ == '__main__':
    unittest.main()